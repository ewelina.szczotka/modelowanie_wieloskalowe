package sample;

import javafx.scene.paint.Color;

import java.util.*;

public class CellularAutomaton {

    private int rows;
    private int columns;
    private int tempRow;
    private int tempColumn;
    private int radius;
    private double cellSize;
    private Random random;
    private String bc;
    private String neighbourhood;
    private Cell[][] grid;
    private ArrayList<Cell> neighbours = new ArrayList<>();
    private HashMap<Color,Integer> neighbourColors = new HashMap<>();


    public CellularAutomaton(int rows, int columns, Cell[][] grid, double cellSize){
        this.rows = rows;
        this.columns = columns;
        this.grid = grid;
        this.cellSize = cellSize;
    }

    public void nextStep(){
        checkStates();
        updateBoard();
    }

    public void checkStates(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                if(!grid[i][j].getState()) {
                    addNeighbours(i,j);
                    updateCell(i,j);
                }
            }
        }
    }

    public void addNeighbours(int row, int column){
        neighbours.clear();

        if(neighbourhood.equals("von Neumann")) {
            vonNeumann(row,column);
        }
        else if(neighbourhood.equals("Moore")){
            moore(row,column);
        }
        else if(neighbourhood.equals("heksagonalne prawe")) {
            hexRight(row,column);
        }
        else if(neighbourhood.equals("heksagonalne lewe")){
            hexLeft(row,column);
        }
        else if(neighbourhood.equals("heksagonalne losowe")) {
            random = new Random();
            int x = random.nextInt(2);
            if(x==0){ hexRight(row,column); }
            else{ hexLeft(row,column); }
        }
        else if(neighbourhood.equals("pentagonalne losowe")){
            random = new Random();
            int x = random.nextInt(4);
            if(x==0) pentLeft(row,column);
            else if(x==1) pentRight(row,column);
            else if(x==2) pentDown(row,column);
            else pentUp(row,column);
        }
        else if(neighbourhood.equals("z promieniem")){
            withRadius(row,column);
        }
    }

    public void updateCell(int row, int column){
        for(int i = 0; i < neighbours.size(); i++){
            if(neighbours.get(i).getState()){
                Color color = neighbours.get(i).getColor();
                int count = neighbourColors.containsKey(color) ? neighbourColors.get(color) : 0;
                neighbourColors.put(color, count + 1);
            }
        }
        if(!neighbourColors.isEmpty()){
            Color color = Collections.max(neighbourColors.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
            grid[row][column].setNewColor(color);
            grid[row][column].setNewState(true);
            neighbourColors.clear();
        }
    }

    public void updateBoard(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                if(!grid[i][j].getState()) {
                    grid[i][j].updateNewState();
                    grid[i][j].updateNewColor();
                }
            }
        }
    }

    public void pentLeft(int row, int column){
        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=0;j++) {
                tempRow = row + i;
                tempColumn = column + j;
                checkBC(row, column, i, j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentRight(int row, int column){
        for(int i = -1; i<=1; i++){
            for(int j = 0; j<=1;j++){
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentUp(int row, int column){
        for(int i = -1; i<=0; i++){
            for(int j=-1; j<=1;j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentDown(int row, int column){
        for(int i = 0; i<=1; i++){
            for(int j=-1; j<=1;j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void hexLeft(int row, int column){
        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=1;j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if((i!=-1&&j!=1)||(i!=1&&j!=-1)) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void hexRight(int row, int column){
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if ((i != -1 && j != -1) || (i != 1 && j != 1)) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void moore(int row, int column){
        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=1;j++){
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void vonNeumann(int row, int column){
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if ((i == 0 && j == -1) || (i == 1 && j == 0) || (i==-1&&j==0)||(i==0&&j==1) ) {
                    tempRow = row+i;
                    tempColumn = column+j;
                    checkBC(row,column,i,j);
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void withRadius(int row, int column){

        double width = radius*cellSize;
        double distance;
        int a = radius;
        for(int i = -a; i<=a; i++){
            for(int j = -a; j<=a; j++) {
                tempRow = row + i;
                tempColumn = column + j;
                checkBC(row,column,i,j);

                double x = grid[row][column].getCenterX()*(cellSize/2);
                double y = grid[row][column].getCenterY()*(cellSize/2);
                double x1 = grid[tempRow][tempColumn].getCenterX()*(cellSize/2);
                double y1 = grid[tempRow][tempColumn].getCenterY()*(cellSize/2);

                distance = Math.sqrt((Math.pow((x - (x1+i*cellSize)), 2) + Math.pow((y - (y1+j*cellSize)), 2)));

                if (distance <= width) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }

    }

    public void checkBC(int row, int column, int i, int j){
        if(bc.equals("periodyczne")) {
            if ((row + i) < 0) {
                tempRow = row + i + rows;
            }
            if ((row + i) >= rows) {
                tempRow = row + i - rows;
            }
            if ((column + j) < 0) {
                tempColumn = column + j + columns;
            }
            if ((column + j) >= columns) {
                tempColumn = column + j - columns;
            }
        }
        else if(bc.equals("absorbujące")) {
            if ((row + i) < 0) {
                tempRow = 0;
            }
            if ((row + i) >= rows) {
                tempRow = row;
            }
            if ((column + j) < 0) {
                tempColumn = 0;
            }
            if ((column + j) >= columns) {
                tempColumn = column;
            }
        }

    }

    public boolean checkFinished(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                if(!grid[i][j].getState()) {
                    return false;
                }
            }
        }
        return true;
    }

    public void updateGrid(Cell[][] grid, int rows, int columns){
        this.grid = grid;
        this.rows = rows;
        this.columns = columns;
    }

    public void update(String neighbourhood, String bc, int radius){
        this.neighbourhood = neighbourhood;
        this.bc = bc;
        this.radius = radius;
    }

    public void updateRadius(int radius){
        this.radius = radius;
    }

    public Cell[][] getGrid(){
        return grid;
    }
}
