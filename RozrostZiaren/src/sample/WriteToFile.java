package sample;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class WriteToFile {

    private File file;
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;
    private LocalDateTime currentTime;

    public WriteToFile(){

    }

    public void start(){
        file = new File(getDate() +".txt");
        fileWriter = null;
        bufferedWriter = null;
        try{
            if(file.createNewFile()){
                System.out.println("Created new file");
            }
            else {
                System.out.println("File already exists");
            }
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public void stop(){
        try {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (fileWriter != null) {
                fileWriter.close();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void write(String message){
        try {
            bufferedWriter.write(message+ "\r\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private final String getDate(){
        currentTime = LocalDateTime.now();
        String date = currentTime.format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss"));
        return date;
    }

}
