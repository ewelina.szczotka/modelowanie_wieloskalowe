package sample;

import javafx.scene.paint.Color;

public class Cell {

    private boolean isSeeded = false;
    private boolean newState;
    private Color color = Color.WHITE;
    private Color newColor = Color.WHITE;
    private double centerX = 0.0;
    private double centerY = 0.0;
    private int energy = 0;
    private double ro = 0;
    private boolean onBound = false;
    private boolean isRecrystallized = false;
    private boolean newRecrystallized = false;
    public double newRo;

    public void setState(boolean state){
        isSeeded = state;
    }

    public void setNewState( boolean state){
        newState = state;
    }

    public void setColor(Color color){ this.color = color; }

    public void setNewColor(Color color){ newColor = color; }

    public void setEnergy(int energy) { this.energy = energy; }

    public void updateNewState(){ isSeeded = newState; }

    public void updateNewColor(){ color = newColor; }

    public boolean getState(){ return isSeeded; }

    public Color getColor(){ return color; }

    public Color getNewColor() { return newColor; }

    public void setCenter( double centerX, double centerY ) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public double getCenterX () { return centerX; }
    public double getCenterY () { return centerY; }

    public int getEnergy() { return energy;}

    public void setRo( double ro ) { this.ro = ro; }

    public double getRo(){ return ro; }

    public void setOnBound(boolean onBound){ this.onBound = onBound;}
    public boolean isOnBound() { return onBound;}

    public void setRecrystallized(boolean recrystallized){ this.isRecrystallized = recrystallized;}
    public boolean isRecrystallized() { return isRecrystallized; }
    public boolean newRecrystallized() { return newRecrystallized; }
    public void updateRecrystallized () { isRecrystallized = newRecrystallized; ro=newRo;}
    public void setRec(boolean rec) { this.newRecrystallized = rec; }
    public void setNewRo(double ro) { newRo = ro;}
    public void addRo(double ro) { this.ro+=ro; }

}
