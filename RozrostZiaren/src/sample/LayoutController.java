package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.util.Duration;


public class LayoutController {

    @FXML
    FlowPane basePane;

    @FXML
    Spinner<Integer> widthSpinner;
    @FXML
    Spinner<Integer> heightSpinner;
    @FXML
    ComboBox bcBox;

    @FXML
    ComboBox nucleationBox;
    @FXML
    Spinner<Integer> inRowSpinner;
    @FXML
    Spinner<Integer> inColSpinner;
    @FXML
    Spinner<Integer> numberSpinner;
    @FXML
    Spinner<Integer> nRadSpinner;
    @FXML
    Button seedBtn;
    @FXML
    ComboBox neighbourhoodBox;
    @FXML
    Spinner<Integer> radSpinner;
    @FXML
    Button startBtn;
    @FXML
    Button stopBtn;
    @FXML
    Button resetBtn;

    @FXML
    RadioButton microBtn;
    @FXML
    RadioButton energyBtn;

    @FXML
    ComboBox mcNeighbourhood;
    @FXML
    Spinner<Double> ktSpinner;
    @FXML
    Spinner<Integer> iterSpinner;
    @FXML
    Button mcStartBtn;
    @FXML
    Button mcStopBtn;

    @FXML
    Button drxStartBtn;
    @FXML
    Button drxStopBtn;



    private final int PANE_WIDTH = 650;
    private int width;
    private int height;
    private int cellSize;
    private Grid grid;
    private Display display;
    private Timeline timeline;
    private CellularAutomaton cellularAutomaton;
    private MonteCarlo monteCarlo;
    private DRX drx;

    public void initialize(){
        initSpinner();
        width = widthSpinner.getValue();
        height = heightSpinner.getValue();
        getCellSize();
        initBoxes();
        nucleationButtonsState();
        grid = new Grid(height, width, nucleationBox.getValue().toString());
        cellularAutomaton = new CellularAutomaton(height,width,grid.getGrid(),cellSize);
        display = new Display(height, width, cellSize, grid);
        nucleationChange();
        basePane.getChildren().clear();
        basePane.getChildren().add(new Group(display.getTilePane()));

        monteCarlo = new MonteCarlo();
        drx = new DRX(height,width);

    }

    public void updateGrid(){
        grid = new Grid(height, width, nucleationBox.getValue().toString());
        display = new Display(height, width, cellSize, grid);
        cellularAutomaton.updateGrid(grid.getGrid(),height,width);
        nucleationChange();
        basePane.getChildren().clear();
        basePane.getChildren().add(new Group(display.getTilePane()));
    }

    public void initBoxes(){
        bcBox.getItems().addAll("periodyczne","absorbujące");
        bcBox.getSelectionModel().selectFirst();
        nucleationBox.getItems().addAll("jednorodne","z promieniem", "losowe", "ręcznie");
        nucleationBox.getSelectionModel().selectFirst();
        neighbourhoodBox.getItems().addAll("von Neumann","Moore", "heksagonalne lewe", "heksagonalne prawe", "heksagonalne losowe", "pentagonalne losowe", "z promieniem");
        neighbourhoodBox.getSelectionModel().selectFirst();
        mcNeighbourhood.getItems().addAll("von Neumann","Moore", "heksagonalne lewe", "heksagonalne prawe", "heksagonalne losowe", "pentagonalne losowe");
        mcNeighbourhood.getSelectionModel().selectFirst();
    }

    public void caStart(){
        toggleButtons(false);
        cellularAutomaton.update(neighbourhoodBox.getValue().toString(), bcBox.getValue().toString(), radSpinner.getValue());
        timeline = new Timeline(
                new KeyFrame(Duration.millis(150), e -> {
                    cellularAutomaton.nextStep();
                    display();
                    if(cellularAutomaton.checkFinished()){
                        caStop();
                    }
                })
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    public void display(){
        if(microBtn.isSelected()){
            display.displayGrid(grid);
        }else if(energyBtn.isSelected()){
            display.displayEnergy(grid);
        }else{
            display.displayRo(grid);
        }
    }


    public void caStop(){
        toggleButtons(true);
        timeline.stop();
        nucleationButtonsState();
    }

    public void mcStart(){
        mcStartBtn.setDisable(true);
        mcStopBtn.setDisable(false);
        monteCarlo.init(cellularAutomaton.getGrid(), height, width, ktSpinner.getValue(), bcBox.getValue().toString(), mcNeighbourhood.getValue().toString());
        timeline = new Timeline(
                new KeyFrame(Duration.millis(150), e -> {
                    monteCarlo.nextStep();
                    display();
                })
        );
        timeline.setCycleCount(iterSpinner.getValue());
        timeline.play();
        timeline.setOnFinished(e -> {
            mcStartBtn.setDisable(false);
            mcStopBtn.setDisable(true);
            monteCarlo.setOnBound();
            drxStartBtn.setDisable(false);
        });
    }

    public void drxStart(){
        drx.update(grid.getGrid(), height, width);
        drxStartBtn.setDisable(true);
        drxStopBtn.setDisable(false);
        drx.start();
        timeline = new Timeline(
                new KeyFrame(Duration.millis(50), e -> {
                    drx.nextStep();
                    display();
                })
        );
        timeline.setCycleCount(201);
        timeline.play();
        timeline.setOnFinished(e -> {
            drx.stop();
            drxStartBtn.setDisable(false);
            drxStopBtn.setDisable(true);
        });
    }

    public void drxStop(){
        drxStartBtn.setDisable(false);
        drxStopBtn.setDisable(true);
        drx.stop();
        timeline.stop();
    }

    public void mcStop(){
        mcStartBtn.setDisable(false);
        mcStopBtn.setDisable(true);
        timeline.stop();
        nucleationButtonsState();
    }


    public void numberChange(){
        grid.setNumber(numberSpinner.getValue());
        grid.nucleation();
        display.displayGrid(grid);
    }

    public void equalSeedsChange(){
        grid.setSeedsInRow(inRowSpinner.getValue());
        grid.setSeedsInColumn(inColSpinner.getValue());
        grid.nucleation();
        display.displayGrid(grid);
    }

    public void radiusChange(){
        grid.setRadius(nRadSpinner.getValue());
        grid.nucleation();
        display.displayGrid(grid);
    }


    public void nucleationChange(){
        String nucleation = nucleationBox.getValue().toString();
        grid.setNucleation(nucleation);
        if(nucleation.equals("jednorodne")){
            inRowSpinner.setDisable(false);
            inColSpinner.setDisable(false);
            numberSpinner.setDisable(true);
            nRadSpinner.setDisable(true);
            seedBtn.setDisable(true);
            equalSeedsChange();
        }
        else if(nucleation.equals("z promieniem")){
            inRowSpinner.setDisable(true);
            inColSpinner.setDisable(true);
            numberSpinner.setDisable(false);
            nRadSpinner.setDisable(false);
            seedBtn.setDisable(false);
            grid.setNumber(numberSpinner.getValue());
            radiusChange();
        }
        else if(nucleation.equals("losowe")){
            inRowSpinner.setDisable(true);
            inColSpinner.setDisable(true);
            numberSpinner.setDisable(false);
            nRadSpinner.setDisable(true);
            seedBtn.setDisable(false);
            numberChange();
        }
        else if(nucleation.equals("ręcznie")){
            inRowSpinner.setDisable(true);
            inColSpinner.setDisable(true);
            numberSpinner.setDisable(true);
            nRadSpinner.setDisable(true);
            seedBtn.setDisable(false);
            grid.nucleation();
            display.displayGrid(grid);
        }
    }

    public void getRandomSeed(){
        grid.getRandomSeed();
        display.displayGrid(grid);
    }


    public void sizeChange(){
        width = widthSpinner.getValue();
        height = heightSpinner.getValue();
        getCellSize();
        updateGrid();
    }

    public void toggleButtons(boolean isEnabled){
        widthSpinner.setDisable(!isEnabled);
        heightSpinner.setDisable(!isEnabled);
        bcBox.setDisable(!isEnabled);
        nucleationBox.setDisable(!isEnabled);
        inRowSpinner.setDisable((!isEnabled));
        inColSpinner.setDisable(!isEnabled);
        numberSpinner.setDisable(!isEnabled);
        nRadSpinner.setDisable(!isEnabled);
        neighbourhoodBox.setDisable(!isEnabled);
        seedBtn.setDisable(!isEnabled);
        iterSpinner.setDisable(!isEnabled);
        ktSpinner.setDisable(!isEnabled);
        radSpinner.setDisable(!isEnabled);
        mcNeighbourhood.setDisable(!isEnabled);
        mcStartBtn.setDisable(!isEnabled);
        startBtn.setDisable(!isEnabled);
        stopBtn.setDisable(isEnabled);
    }

    public void reset(){
        grid.initializeGrid();
        grid.nucleation();
        cellularAutomaton.updateGrid(grid.getGrid(),height,width);
        display.displayGrid(grid);
        drxStartBtn.setDisable(true);
    }

    public void getCellSize(){
        int max;
        if(height>=width) max = height;
        else max = width;
        cellSize = PANE_WIDTH/max;
    }

    public void nucleationButtonsState(){
        String nucleation = nucleationBox.getValue().toString();
        if(nucleation.equals("jednorodne")){
            inRowSpinner.setDisable(false);
            inColSpinner.setDisable(false);
            numberSpinner.setDisable(true);
            nRadSpinner.setDisable(true);
        }
        else if(nucleation.equals("z promieniem")){
            inRowSpinner.setDisable(true);
            inColSpinner.setDisable(true);
            numberSpinner.setDisable(false);
            nRadSpinner.setDisable(false);
        }
        else if(nucleation.equals("losowe")){
            inRowSpinner.setDisable(true);
            inColSpinner.setDisable(true);
            numberSpinner.setDisable(false);
            nRadSpinner.setDisable(true);
        }
        else if(nucleation.equals("ręcznie")){
            inRowSpinner.setDisable(true);
            inColSpinner.setDisable(true);
            numberSpinner.setDisable(true);
            nRadSpinner.setDisable(true);
        }
    }

    public void initSpinner(){
        widthSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,1000,300,1));
        widthSpinner.valueProperty().addListener((obs)->{
            sizeChange();
        });
        heightSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,1000,300,1));
        heightSpinner.valueProperty().addListener((obs)->{
            sizeChange();
        });
        ktSpinner.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0.1,6, 0.6,0.1));
        iterSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,1000,15));
        radSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,10));
        inRowSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,10));
        inRowSpinner.valueProperty().addListener((obs)->{
            equalSeedsChange();
        });
        inColSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,9));
        inColSpinner.valueProperty().addListener((obs)->{
            equalSeedsChange();
        });
        numberSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,25));
        numberSpinner.valueProperty().addListener((obs)->{
            numberChange();
        });
        nRadSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,10));
        nRadSpinner.valueProperty().addListener((obs)->{
            cellularAutomaton.updateRadius(nRadSpinner.getValue());
        });
    }

}
