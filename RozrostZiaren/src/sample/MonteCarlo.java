package sample;

import java.util.*;

public class MonteCarlo {

    private Cell[][] grid;
    private int rows;
    private int columns;
    private int tempRow;
    private int tempColumn;

    private String neighbourhood;
    private String bc;
    private double kt;
    private Random random;

    ArrayList<Integer> index = new ArrayList<>();
    ArrayList<Cell> neighbours = new ArrayList<>();


    public MonteCarlo(){}

    public void init(Cell[][] grid, int rows, int columns, double kt, String bc, String neighbourhood){
        index.clear();
        this.rows = rows;
        this.columns = columns;
        for(int i=0;i<rows*columns; i++){
            index.add(i);
        }
        this.grid = grid;
        this.kt = kt;
        this.bc = bc;
        this.neighbourhood = neighbourhood;
    }

    public void nextStep(){

        Collections.shuffle(index);

        for(int i = 0; i<rows*columns; i++){

            int d = index.get(i);
            int row = d / columns;
            int column = d % rows;

            addNeighbours(row,column);

            int energyBefore = countEnergyBefore(row,column);
            int energyAfter = countEnergyAfter(row,column);
            int dEnergy = energyAfter - energyBefore;

            if(dEnergy<=0){
                grid[row][column].updateNewColor();
                grid[row][column].setEnergy(energyAfter);
            }
            else{
                random = new Random();
                double p = Math.exp(-(dEnergy/kt));
                double x = random.nextDouble();
                if (x<p){
                    grid[row][column].updateNewColor();
                    grid[row][column].setEnergy(energyAfter);
                }
                else{
                    grid[row][column].setEnergy(energyBefore);
                }
            }

        }

    }

    public int countEnergyBefore(int row, int column){
        int energy = 0;
        for(int i = 0; i<neighbours.size(); i++){
            if(neighbours.get(i).getColor() != grid[row][column].getColor()){
                energy++;
            }
        }
        return energy;
    }

    public int countEnergyAfter(int row, int column){
        int energy = 0;
        Collections.shuffle(neighbours);
        grid[row][column].setNewColor(neighbours.get(0).getColor());
        for(int i = 0; i<neighbours.size(); i++){
            if(neighbours.get(i).getColor() != grid[row][column].getNewColor()){
                energy++;
            }
        }
        return energy;
    }


    public void addNeighbours(int row, int column){

        neighbours.clear();

        if(neighbourhood.equals("von Neumann")) {
            vonNeumann(row,column);
        }
        else if(neighbourhood.equals("Moore")){
            moore(row,column);
        }
        else if(neighbourhood.equals("heksagonalne prawe")) {
            hexRight(row,column);
        }
        else if(neighbourhood.equals("heksagonalne lewe")){
            hexLeft(row,column);
        }
        else if(neighbourhood.equals("heksagonalne losowe")) {
            random = new Random();
            int x = random.nextInt(2);
            if(x==0){ hexRight(row,column); }
            else{ hexLeft(row,column); }
        }
        else if(neighbourhood.equals("pentagonalne losowe")){
            random = new Random();
            int x = random.nextInt(4);
            if(x==0) pentLeft(row,column);
            else if(x==1) pentRight(row,column);
            else if(x==2) pentDown(row,column);
            else pentUp(row,column);
        }
        else if(neighbourhood.equals("z promieniem")){
            moore(row,column);
        }
    }


    public void pentLeft(int row, int column){
        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=0;j++) {
                if(i==0&&j==0){
                    break;
                }
                tempRow = row + i;
                tempColumn = column + j;
                checkBC(row, column, i, j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentRight(int row, int column){
        for(int i = -1; i<=1; i++){
            for(int j = 0; j<=1;j++){
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentUp(int row, int column){
        for(int i = -1; i<=0; i++){
            for(int j=-1; j<=1;j++) {
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void pentDown(int row, int column){
        for(int i = 0; i<=1; i++){
            for(int j=-1; j<=1;j++) {
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void hexLeft(int row, int column){
        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=1;j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if((i!=-1&&j!=1)||(i!=1&&j!=-1)) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void hexRight(int row, int column){

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if ((i != -1 && j != -1) || (i != 1 && j != 1) ) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }
    }

    public void vonNeumann(int row, int column){
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                if ((i != -1 && j != -1) || (i != 1 && j != 1) || (i!=-1&&j!=1)||(i!=1&&j!=-1) ) {
                    neighbours.add(grid[tempRow][tempColumn]);
                }
            }
        }

    }

    public void moore(int row, int column){

        for(int i = -1; i<=1; i++){
            for(int j=-1; j<=1;j++){
                if(i==0&&j==0){
                    break;
                }
                tempRow = row+i;
                tempColumn = column+j;
                checkBC(row,column,i,j);
                neighbours.add(grid[tempRow][tempColumn]);
            }
        }
    }

    public void checkBC(int row, int column, int i, int j){
        if(bc.equals("periodyczne")) {
            if ((row + i) < 0) {
                tempRow = row + i + rows;
            }
            if ((row + i) >= rows) {
                tempRow = row + i - rows;
            }
            if ((column + j) < 0) {
                tempColumn = column + j + columns;
            }
            if ((column + j) >= columns) {
                tempColumn = column + j - columns;
            }
        }
        else if(bc.equals("absorbujące")) {
            if ((row + i) < 0) {
                tempRow = 0;
            }
            if ((row + i) >= rows) {
                tempRow = row;
            }
            if ((column + j) < 0) {
                tempColumn = 0;
            }
            if ((column + j) >= columns) {
                tempColumn = column;
            }
        }

    }

    public void setOnBound(){
        for(int i = 0; i <rows; i++){
            for(int j = 0; j <columns; j++){
                if (grid[i][j].getEnergy() > 0) {
                    grid[i][j].setOnBound(true);
                } else {
                    grid[i][j].setOnBound(false);
                }
            }
        }

        for(int i = 0; i <rows; i++){
            for(int j = 0; j <columns; j++){
                grid[i][j].setOnBound(false);
                neighbours.clear();
                moore(i,j);
                for(int x = 0; x<neighbours.size(); x++){
                    if(neighbours.get(x).getColor() != grid[i][j].getColor()){
                        grid[i][j].setOnBound(true);
                        break;
                    }
                }
            }
        }
    }

}
