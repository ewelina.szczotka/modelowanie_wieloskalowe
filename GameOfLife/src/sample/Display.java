package sample;

import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Display {

    private TilePane tilePane = new TilePane();
    private int boardHeight;
    private int boardWidth;
    private int size;


    public Display(int boardHeight, int boardWidth, int width, Grid grid){

        tilePane.getChildren().clear();

        tilePane.setPrefColumns(boardWidth);
        tilePane.setPrefRows(boardHeight);
        tilePane.setHgap(1);
        tilePane.setVgap(1);

        this.boardHeight = boardHeight;
        this.boardWidth = boardWidth;
        if(boardHeight>=boardWidth) size = boardHeight;
        else size = boardWidth;
        int cellSize = width/size;
        Cell[][] cells = grid.getGrid();
        for(int i = 0; i < boardHeight; i++){
            for(int j = 0; j < boardWidth; j++){
                Color color = cells[i][j].getState() ? Color.DARKGRAY : Color.WHITE;
                Rectangle rectangle = new Rectangle(cellSize, cellSize, color);
                tilePane.getChildren().add(rectangle);
                attachListener(rectangle, cells[i][j]);
            }
        }

    }

    public void displayGrid(Grid grid){
        Cell[][] cells = grid.getGrid();
        for(int i = 0; i < boardHeight; i++){
            for(int j = 0; j < boardWidth; j++){
                Rectangle rectangle = (Rectangle)tilePane.getChildren().get(i*boardWidth+j);
                rectangle.setFill(cells[i][j].getState() ? Color.DARKGREY : Color.WHITE);
            }
        }
    }

    public void attachListener(Rectangle rectangle, Cell cell){
        rectangle.setOnMouseClicked(e -> {
            rectangle.setFill(cell.getState() ? Color.DARKGREY : Color.WHITE);
            cell.setState(!cell.getState());
        });
    }

    public TilePane getTilePane(){
        return tilePane;
    }


}
