package sample;

public class Grid {

    private int rows;
    private int columns;
    private Cell[][] grid;

    public Grid(int rows, int columns, String state){

        this.rows = rows;
        this.columns = columns;
        initializeCells(state);

    }

    public void initializeCells(String state){

        grid = new Cell[rows][columns];

        if(state.equals("niezmienne")){
            for(int i = 0; i < rows; i++){
                for(int j = 0; j < columns; j++){
                    grid[i][j] = new Cell();
                }
            }
            int startRow = rows/3;
            int startCol = columns/3;
            grid[startRow][startCol].setState(true);
            grid[startRow][startCol+3].setState(true);
            grid[startRow-1][startCol+1].setState(true);
            grid[startRow+1][startCol+1].setState(true);
            grid[startRow-1][startCol+2].setState(true);
            grid[startRow+1][startCol+2].setState(true);

        }
        else if(state.equals("oscylator")){
            for(int i = 0; i < rows; i++){
                for(int j = 0; j < columns; j++){
                    grid[i][j] = new Cell();
                }
            }
            int startRow = rows/3;
            int startCol = columns/3;
            grid[startRow][startCol].setState(true);
            grid[startRow-1][startCol].setState(true);
            grid[startRow-2][startCol].setState(true);
        }
        else if(state.equals("glider")){
            for(int i = 0; i < rows; i++){
                for(int j = 0; j < columns; j++){
                    grid[i][j] = new Cell();
                }
            }
            int startRow = rows/3;
            int startCol = columns/3;
            grid[startRow][startCol].setState(true);
            grid[startRow][startCol+1].setState(true);
            grid[startRow-1][startCol+1].setState(true);
            grid[startRow-1][startCol+2].setState(true);
            grid[startRow+1][startCol+2].setState(true);

        }
        else if(state.equals("losowy")){
            for(int i = 0; i < rows; i++){
                for(int j = 0; j < columns; j++){
                    grid[i][j] = new Cell();
                    int random = (int )(Math.random() * 100 + 1);
                    if(random>90) grid[i][j].setState(true);
                }
            }
        }
        else if(state.equals("ręcznie")){
            for(int i = 0; i < rows; i++){
                for(int j = 0; j < columns; j++){
                    grid[i][j] = new Cell();
                }
            }
        }

    }

    public void nextStep(){
        checkStates();
        updateBoard();
    }

    public void checkStates(){

        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                int number = countNeighbours(i,j);
                if(number < 2 || number > 3) grid[i][j].setNewState(false);
                else if(number==3) grid[i][j].setNewState(true);
                else if(number==2) grid[i][j].setNewState(grid[i][j].getState());
            }
        }

    }

    private int countNeighbours(int row, int column){

        int number = 0;

            for(int i = -1; i<=1; i++){
                for(int j=-1; j<=1;j++){

                    int ii = i;
                    int jj = j;

                    if (row == 0) {
                        if (i == -1) {
                            ii = rows - 1;
                        }
                    }
                    if (row == rows - 1) {
                        if (i == 1) {
                            ii -=rows;
                        }
                    }
                    if (column==0){
                        if (j == -1) {
                            jj = columns-1;
                        }
                    }
                    if(column==columns-1){
                        if (j == 1) {
                            jj -=columns;
                        }
                    }

                    if((i!=0)||(j!=0)){
                        if (grid[row+ii][column+jj].getState()){
                            number++;
                        }
                    }
                }
            }

        return number;
    }


    public void updateBoard(){

        for(int i = 0; i < rows; i++){
            for(int j = 0; j< columns; j++){
                grid[i][j].updateNewState();
            }
        }

    }

    public Cell[][] getGrid(){
        return grid;
    }

}
