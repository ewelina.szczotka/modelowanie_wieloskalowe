package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.util.Duration;


public class LayoutController {

    @FXML
    FlowPane basePane;
    @FXML
    TextField widthTxt;
    @FXML
    TextField heightTxt;
    @FXML
    ComboBox stateBox;
    @FXML
    Button startBtn;
    @FXML
    Button stopBtn;
    @FXML
    Button resetBtn;

    private int width;
    private int height;
    private String state;
    private Grid grid;
    private Display display;
    private Timeline timeline;

    public void initialize(){
        width = Integer.parseInt(widthTxt.getText());
        height = Integer.parseInt(heightTxt.getText());
        stateBox.getItems().addAll("niezmienne","oscylator","glider","losowy", "ręcznie");
        stateBox.getSelectionModel().selectFirst();
        state = stateBox.getValue().toString();
        redraw();
    }

    public void stateChange(){
        state = stateBox.getValue().toString();
        redraw();
    }

    public void sizeChange(){
        width = Integer.parseInt(widthTxt.getText());
        height = Integer.parseInt(heightTxt.getText());
        redraw();
    }

    public void reset(){
        state = stateBox.getValue().toString();
        width = Integer.parseInt(widthTxt.getText());
        height = Integer.parseInt(heightTxt.getText());
        redraw();
    }

    public void redraw(){
        grid = new Grid(height,width, state);
        display = new Display(height, width, 650, grid);
        basePane.getChildren().clear();
        basePane.getChildren().add(new Group(display.getTilePane()));
        display.displayGrid(grid);
    }

    public void toggleButtons(boolean isEnabled){
        widthTxt.setDisable(!isEnabled);
        heightTxt.setDisable(!isEnabled);
        stateBox.setDisable(!isEnabled);
        resetBtn.setDisable(!isEnabled);
        startBtn.setDisable(!isEnabled);
        stopBtn.setDisable(isEnabled);
    }

    public void start(){
        toggleButtons(false);
        timeline = new Timeline(
                new KeyFrame(Duration.millis(200), e -> {
                    grid.nextStep();
                    display.displayGrid(grid);
                })
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    public void stop(){
        toggleButtons(true);
        timeline.stop();
    }

}
