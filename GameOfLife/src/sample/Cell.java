package sample;

public class Cell {

    private Boolean isAlive = false;
    private Boolean newState;

    public void setState(boolean state){
        isAlive = state;
    }

    public void setNewState( boolean state){
        newState = state;
    }

    public void updateNewState(){
        isAlive = newState;
    }

    public Boolean getState(){
        return isAlive;
    }

}
